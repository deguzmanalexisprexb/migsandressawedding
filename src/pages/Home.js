import { Container, Row, Col } from 'react-bootstrap';
import { useEffect } from 'react';

import Video from '../components/Video';
import homeBg from '../assets/background/4.JPG';
import monogram from '../assets/logo/editedlogo1.png';

const Home = () => {
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    // Check if the user's device is windows or android
    const isAndroid = /Android/i.test(navigator.userAgent);
    const isWindows = /Windows/.test(navigator.userAgent);
    
    return (
    	<section className="pt-5 text-center min-vh-100" style={{background: 'linear-gradient(to right, #ACC4DA, #ACC4DA)'}}>
        
            <div
                className="text-center h-100 d-flex flex-column align-items-md-start justify-content-center min-vh-100"
                style={{
                    backgroundImage:
                        `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${homeBg})`,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
                }}
            >
                <Container className="py-5">
                    <Row>
                        <Col lg={8} md={10} className="mx-auto">
                            <h1 className="mb-2 amaticSc fw-bold title">Save The Date</h1>
                            <p className="mb-0 lead dancingScript fw-bold">
                                11 . 14 . 2023
                            </p>
                            <i className='h1 bx bx-calendar-heart bx-tada'></i>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={8} md={10} className="mx-auto">
                            <img src={monogram} alt="" className='mw-100 pe-3' style={{height: '300px'}} />
                        </Col>
                    </Row>
                </Container>
                <Container>
                    <Row>
                        <Col lg={8} md={10} className="mx-auto pb-5 pb-md-3">
                            <p className="lead dancingScript fw-bold">
                                When the time is right, I, The lord will make it happen.
                            </p>
                            <p className="lead mb-4 dancingScript fw-bold">Isiah 60:22</p>
                        </Col>
                    </Row>
                </Container>
            </div>
         
            <Video /> 

            <div
                className="py-md-5 text-center h-100 d-flex align-items-center min-vh-100"
                style={{
                    backgroundImage:
                        `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${homeBg})`,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
                }}
            >
                <Container className="py-5">
                    <Row>
                        <Col lg={8} md={10} className="mx-auto">
                            <p className="lead dancingScript">With the grace of the Lord and blessing of our parents,</p>
                            <p className="h3 cormorantGaramond fw-bold">
                                Mr Gerardo Joaquin and Mrs Emelyn Antonio
                            </p>
                            <p className="lead dancingScript">
                                and
                            </p>
                            <p className="h3 cormorantGaramond fw-bold">
                                Capt. Moises Barrientos and Mrs Teresa Barrientos
                            </p>
                            <p className="lead dancingScript">
                                we,
                            </p>
                            <p className="display-3 cormorantGaramond fw-bold">
                                Migs
                            </p>
                            <p className="lead dancingScript">
                                and
                            </p>
                            <p className="display-3 cormorantGaramond fw-bold">
                                Ressa
                            </p>
                            <p className="lead dancingScript">
                                request the honor of your presence as we unite in the sacrament of holy matrimony.
                            </p>
                            <p className="h2 cormorantGaramond fw-bold">
                                November
                            </p>
                            <p className="lead cormorantGaramond fw-bold">
                                Tuesday | 14th | at 2:00 PM
                            </p>
                            <p className="mb-4 h2 cormorantGaramond fw-bold">2023</p>
                        </Col>
                    </Row>
                </Container>
            </div>  
      	</section>


    );
};

export default Home;