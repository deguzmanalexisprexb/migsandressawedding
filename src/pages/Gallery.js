import { Container, Row, Col } from 'react-bootstrap';
import { useEffect } from 'react';

import galleryBg from '../assets/background/4.JPG';
import Album from '../components/Album'

const Gallery = () => {

	useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    // Check if the user's device is windows or android
    const isAndroid = /Android/i.test(navigator.userAgent);
    const isWindows = /Windows/.test(navigator.userAgent);
    
    return (
    	<section style={{background: 'linear-gradient(to bottom, #ACC4DA, white)'}}>
    		<div
    		    className="py-md-5 text-center h-100 d-flex align-items-center min-vh-100"
    		    style={{
    		        backgroundImage:
    		            `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${galleryBg})`,
    		        backgroundPosition: 'center',
    		        backgroundSize: 'cover',
                    backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
    		    }}
    		>
    		    <Container className="py-5">
    		        <Row>
    		            <Col lg={8} md={10} className="mx-auto">
    		                <h1 className="title mb-4 amaticSc fw-bold">Gallery</h1>
                            <p className="lead mb-4 dancingScript fw-bold">
                                Capturing Love, One Frame at a Time
                            </p>
    		            </Col>
    		        </Row>
    		    </Container>
    		</div>

            <Container className="pt-5 pb-4 text-center">
                <Row>
                    <Col lg={8} md={10} className="mx-auto mt-5">
                        <h1 className="display-1 mb-0 greatVibes fw-bold text-white">Our Magical Journey</h1>
                    </Col>
                </Row>
            </Container>

            <Album />

    	</section>
    );
};

export default Gallery;