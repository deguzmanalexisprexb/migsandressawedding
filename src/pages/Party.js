import { Container, Row, Col, Carousel } from 'react-bootstrap';
import { useEffect, useState } from 'react';

import partyBg from '../assets/background/4.JPG';
import invitation3 from '../assets/invitation-3.PNG';

const Party = () => {

    const [maleImages, setMaleImages] = useState([]);
    const [femaleImages, setFemaleImages] = useState([]);

	useEffect(() => {
        window.scrollTo(0, 0);

        // Dynamically import images from the 'images' folder
        const importImages = async () => {
          const imageFiles = require.context('../assets/dresscode/male', false, /\.(jpg|jpeg|png|gif|svg|PNG)$/);
          const images = imageFiles.keys().map(imageFiles);
          setMaleImages(images);

          const imageFiles1 = require.context('../assets/dresscode/female', false, /\.(jpg|jpeg|png|gif|svg|PNG)$/);
          const images1 = imageFiles1.keys().map(imageFiles1);
          setFemaleImages(images1);
        };

        importImages();
    }, []);

    // Check if the user's device is windows or android
    const isAndroid = /Android/i.test(navigator.userAgent);
    const isWindows = /Windows/.test(navigator.userAgent);
    
    return (
    	<section>
    		<div
    		    className="py-md-5 text-center h-100 d-flex align-items-center min-vh-100"
    		    style={{
    		        backgroundImage:
    		            `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${partyBg})`,
    		        backgroundPosition: 'center',
    		        backgroundSize: 'cover',
                    backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
    		    }}
    		>
    		    <Container className="py-5">
    		        <Row>
    		            <Col lg={8} md={10} className="mx-auto">
    		                <h1 className="title mb-4 amaticSc fw-bold">Dress Code</h1>
                            <p className="lead mb-4 dancingScript fw-bold">
                                Selecting the Right Outfit for Our Wedding
                            </p>
    		            </Col>
    		        </Row>
    		    </Container>
    		</div>
            <Container >
                <Row className='my-4'>
                    <Col className='text-center'>
                        <img src={invitation3} alt="" className='mw-100'/>
                    </Col>
                </Row>
                <Row className='justify-content-center'>
                    <Col md={8} lg={5} className="p-3">
                        <h2 className='text-center'>Gentlemen</h2>
                        <Carousel id="carousel1" interval={2000}>
                        {
                            maleImages.map((image, index) => (
                                <Carousel.Item key={index} >
                                    <img className="d-block w-100" src={image} alt={`Slide ${index}`} />
                                </Carousel.Item>
                            ))  
                        }
                        </Carousel>
                    </Col>
                    <Col md={8} lg={5} className="p-3">
                        <h2 className='text-center'>Ladies</h2>
                        <Carousel id="carousel2" interval={2000}>
                        {
                            femaleImages.map((image, index) => (
                                <Carousel.Item key={index}>
                                    <img className="d-block w-100" src={image} alt={`Slide ${index}`} />
                                </Carousel.Item>
                            ))  
                        }
                        </Carousel>
                    </Col>
                </Row>
            </Container>
    	</section>
    );
};

export default Party;