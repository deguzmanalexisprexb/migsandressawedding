import { Container, Row, Col } from 'react-bootstrap';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';

import faqsBg from '../assets/background/4.JPG';
import { AiOutlineCloseCircle } from 'react-icons/ai';

const Faqs = () => {
    
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    // Check if the user's device is windows or android
    const isAndroid = /Android/i.test(navigator.userAgent);
    const isWindows = /Windows/.test(navigator.userAgent);

    return (
    	<section>
    		<div
    		    className="py-md-5 text-center h-100 d-flex align-items-center min-vh-100"
    		    style={{
    		        backgroundImage:
    		            `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${faqsBg})`,
    		        backgroundPosition: 'center',
    		        backgroundSize: 'cover',
                    backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
    		    }}
    		>
    		    <Container className="py-5">
    		        <Row>
    		            <Col lg={8} md={10} className="mx-auto">
    		                <h1 className="display-1 mb-4 amaticSc fw-bold">Frequently Asked Questions</h1>
    		            </Col>
    		        </Row>
    		    </Container>
    		</div>
            <Container className="py-5 d-flex justify-content-center align-items-center text-center">
                <Row>
                    <Col md={12} className='mx-auto faqsContent' style={{color: '#ACC4DA'}} >
                        <h1>What time should I be there?</h1>
                        <h4>The ceremony will start at exactly 2:00 PM. We ask you to be there one hour before the exact time to avoid delays</h4>

                        <h5>A gentle reminder:</h5>
                        <h4>Don't starve yourself. Please eat your lunch before going to the church.</h4>

                        <h1>How do I get there?</h1>
                        <h5>From Metro Manila:</h5>
                        <h4>Start your journey by taking the South Luzon Expressway (SLEX) heading south. Continue along SLEX until you reach the Santa Rosa Exit. Once you've exited, follow the road signs that guide you towards Silang, Cavite. You'll likely pass through the city of Santa Rosa and then continue your journey on the South Boulevard. Keep following the road, and you will eventually arrive at Ayala Westgrove Heights, where you can find St. Benedict Parish.</h4>
                        <h4>You'll find the exact location on our <Link to="/venue" className='fw-bold' style={{color: '#ACC4DA'}} >Venue Page,</Link> where you can also access directions via Waze or Google Maps. </h4>

                        <h1>Are my kids welcome?</h1>
                        <h4>While we love your little ones, we want you to relax and enjoy the day. We appreciate you for making arrangements ahead of time and leaving kids at home so you can celebrate with us</h4>
                        <h4>We truly wish we could invite all our guests' children to our wedding however we are only able to invite the children of our immediate families</h4>

                        <h1>Can I bring plus one?</h1>
                        <h5>(Jowa, kaibigan, kapatid, drive, yaya, or anyone we are not close to or aquainted to)</h5>
                        <h4>Unfortunately, due to budget restriction, we carefully chose our wedding guests. We don't want unexpected circumstances that will put us into financial distress after the wedding. We only allotted our budget to the people we invited</h4>
                        <h4>Aside from that we are at capacity with our venue which means we are sadly unable to accomodate additional guests. We look forward to celebrating with you on the day!</h4>

                        <h1>How do I reserve a seat?</h1>
                        <h4>For your seat confirmation, you may contact the numbers below.</h4>
                        <h4>Marou Madrid, Events Coordinator 09260943884</h4>
                        <h4>Migs Antonio 09957837797</h4>
                        <h4>Ressa Barrientos 09957837798</h4>

                        <h1>THE DON'TS:</h1>
                        <ul className='d-inline-block list-unstyled '>
                            <li className='d-flex align-items-center justify-content-center cormorantGaramond h3'>
                                <AiOutlineCloseCircle className='me-2'/>No Proposal
                            </li>
                            <li className='d-flex align-items-center justify-content-center cormorantGaramond h3'>
                                <AiOutlineCloseCircle className='me-2'/>No announcement of pregnancy
                            </li>
                            <li className='d-flex align-items-center justify-content-center cormorantGaramond h3'>
                                <AiOutlineCloseCircle className='me-2'/>Gender reveal
                            </li>
                            <li className='d-flex align-items-center justify-content-center cormorantGaramond h3'>
                                <AiOutlineCloseCircle className='me-2'/>Ignore dress code
                            </li>
                        </ul>


                    </Col>
                </Row>
            </Container>
    	</section>
    );
};

export default Faqs;