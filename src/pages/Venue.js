import { Container, Row, Col } from 'react-bootstrap';
import { useEffect } from 'react';
import { FaWaze } from 'react-icons/fa';
import { SiGooglemaps } from 'react-icons/si';
import { Link } from 'react-router-dom';

import venueBg from '../assets/background/4.JPG';
import stBenedict from '../assets/StBenedict.jpg';
import altaVerde from '../assets/alta.jpg';

const Venue = () => {

	useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    
	// Check if the user's device is windows or android
    const isAndroid = /Android/i.test(navigator.userAgent);
    const isWindows = /Windows/.test(navigator.userAgent);

    return (
    	<section className="pt-5 text-center min-vh-100">
    		<div
    		    className="py-md-5 text-center h-100 d-flex align-items-center min-vh-100"
    		    style={{
    		        backgroundImage:
    		            `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${venueBg})`,
    		        backgroundPosition: 'center',
    		        backgroundSize: 'cover',
                    backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
    		    }}
    		>
    		    <Container className="py-5">
    		        <Row>
    		            <Col lg={8} md={10} className="mx-auto">
    		                <h1 className="title mb-4 amaticSc fw-bold">Our Venue</h1>
    		                <p className="lead mb-4 dancingScript fw-bold">
    		                    The Perfect Place for Our Happily Ever After
    		                </p>
    		            </Col>
    		        </Row>
    		    </Container>
    		</div>

    		<div className="py-5">
	            <Container>
	                <Row>
	                    <Col md={6} className='d-flex align-items-center'>
	                        <img className="img-fluid" src={stBenedict} alt="Cover" />
	                    </Col>
	                    <Col md={6} className="px-md-5 p-3 d-flex flex-column justify-content-center">
	                        <h1>Ceremony</h1>
	                        <h4 className="mb-3">St. Benedict Parish | 2:00 PM</h4>
	                        <p className="mb-3 lead">Ayala Westgrove Heights, Brgy. Inchican, South Blvd, Silang Cavite</p>
	                        <p className="mb-2">Choose navigation</p>
	                        <Row className="justify-content-center">
	                            <Col xs={4} className="d-flex justify-content-center">
	                                <Link to="https://www.waze.com/live-map/directions/ph/calabarzon/silang/st.-benedict-parish-hall-and-office?to=place.ChIJf_EV1Qd9vTMRzgQfaIzPaE8" className='px-4' target='_blank' style={{color: '#6F8E88'}} >
	                                    <FaWaze size={30} />
	                                    <p>Waze</p>
	                                </Link>
	                            </Col>
	                            <Col xs={4} className="d-flex justify-content-center">
	                                <Link to="https://www.google.com/maps/dir//62QR%2BR8Q+St.+Benedict+Parish,+South+Blvd,+Silang,+4118+Cavite/@14.1938973,120.8864834,11z/data=!4m8!4m7!1m0!1m5!1m1!1s0x33bd7d0592775e0b:0xcbfe25c5710cb13e!2m2!1d121.0409009!2d14.2396042?entry=ttu" className='px-4' target='_blank' style={{color: '#6F8E88'}} >
	                                    <SiGooglemaps size={30} />
	                                    <p>Google</p>
	                                </Link>
	                            </Col>
	                        </Row>
	                    </Col>

	                </Row>
	            </Container>	       
	        </div>

	        <div className="py-5">
	            <Container>
	                <Row className='d-flex flex-column flex-md-row'>
	                	<Col md={6} className="px-md-5 p-3 d-flex flex-column justify-content-center order-2 order-md-1">
	                	    <h1>Party</h1>
	                	    <h4 className="mb-3">Alta Veranda De Tibig | 4:00 PM</h4>
	                	    <p className="mb-3 lead">Alcalde Street Brgy. Tibig Silang, Cavite</p>
	                	    <p className="mb-2">Choose navigation</p>
	                	    <div className="row justify-content-center">
	                	        <Col xs={4} className="d-flex justify-content-center">
	                	            <Link to="https://www.waze.com/en-GB/live-map/directions/alta-veranda-de-tibig-alcalde-silang?fbclid=IwAR2XcddIXSHERYor9Bvg2Ya1oHLELUmW2TAHt2nHyrACdrtJBrq2ccqVPJ0&to=place.w.79298702.793052559.3922827" className='px-4' target='_blank' style={{color: '#6F8E88'}} >
	                	                <FaWaze size={30} />
	                	                <p>Waze</p>
	                	            </Link>
	                	            
	                	        </Col>
	                	        <Col xs={4} className="d-flex justify-content-center">
	                	        	<Link to="https://www.google.com/maps/dir//alta+veranda+de+tibig/@14.4709734,120.7148432,10z/data=!4m8!4m7!1m0!1m5!1m1!1s0x33bd7c36807252db:0x6924f5eae4afeada!2m2!1d121.007672!2d14.2294845?entry=ttu" className='px-4' target='_blank' style={{color: '#6F8E88'}} >
	                	                <SiGooglemaps size={30} />
	                	                <p>Google</p>
	                	            </Link>
	                	        </Col>
	                	    </div>
	                	</Col>
	                    <Col md={6} className='d-flex align-items-center order-1 order-md-2'>
	                        <img className="img-fluid" src={altaVerde} alt="Cover" />
	                    </Col>
	                </Row>
	            </Container>	       
	        </div>
    	</section>
    );
};

export default Venue;