import { Container, Row, Col } from 'react-bootstrap';
import { useEffect } from 'react';

import entourageBg from '../assets/background/4.JPG';
import invitation2 from '../assets/invitation2.6.jpg';

const Entourage = () => {
	useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    // Check if the user's device is windows or android
    const isAndroid = /Android/i.test(navigator.userAgent);
    const isWindows = /Windows/.test(navigator.userAgent);

    return (
    	<section>
    		<div
    		    className="py-md-5 text-center h-100 d-flex align-items-center min-vh-100"
    		    style={{
    		        backgroundImage:
    		            `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${entourageBg})`,
    		        backgroundPosition: 'center',
    		        backgroundSize: 'cover',
                    backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
    		    }}
    		>
    		    <Container className="py-5">
    		        <Row>
    		            <Col lg={8} md={10} className="mx-auto">
    		                <h1 className="title mb-4 amaticSc fw-bold">Entourage</h1>
                            <p className="lead mb-4 dancingScript fw-bold">
                                Love's A-Team: Where Our VIPs Shine
                            </p>
    		            </Col>
    		        </Row>
    		    </Container>
    		</div>
            <Container >
                <Row className='my-4 d-flex justify-content-center'>
                    <Col md={8} className='text-center'>
                        <img src={invitation2} alt="" className='mw-100'/>
                    </Col>
                </Row>
            </Container>
    	</section>
    );
};

export default Entourage;