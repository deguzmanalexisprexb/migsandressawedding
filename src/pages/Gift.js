import { Container, Row, Col } from 'react-bootstrap';
import { useEffect } from 'react';

import giftBg from '../assets/background/4.JPG';

const Gift = () => {

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    
    // Check if the user's device is windows or android
    const isAndroid = /Android/i.test(navigator.userAgent);
    const isWindows = /Windows/.test(navigator.userAgent);
    
    return (
		<section className="pt-5 text-center min-vh-100">
    		<div
    		    className="py-md-5 text-center h-100 d-flex align-items-center min-vh-100"
    		    style={{
    		        backgroundImage:
    		            `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${giftBg})`,
    		        backgroundPosition: 'center',
    		        backgroundSize: 'cover',
                    backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
    		    }}
    		>
    		    <Container className="py-5">
    		        <Row>
    		            <Col lg={8} md={10} className="mx-auto">
    		                <h1 className="title mb-4 amaticSc fw-bold">Gift Guide</h1>
    		                <p className="lead mb-4 dancingScript fw-bold">
    		                    Wrap Up Love: Our Gift Guide
    		                </p>
    		            </Col>
    		        </Row>
    		    </Container>
    		</div>
            <Container className='d-flex align-items-center' style={{height: '80vh'}}>
                <Row className='my-4'>
                    <Col className='text-center' style={{color: '#ACC4DA'}}>
                        <i class='h1 bx bxs-gift bx-tada' ></i>
                        <h2>A NOTE ON GIFTS</h2>
                        <h4>Your presence at our wedding is present enough but if we are honored with a gift from you, a monetary gift will be very much appreciated.</h4>
                    </Col>
                </Row>
            </Container>
    	</section>
    );
};

export default Gift;		