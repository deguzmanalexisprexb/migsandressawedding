import img1 from '../assets/albumPics/Migs _ Ressa-1.jpg';
import img2 from '../assets/albumPics/Migs _ Ressa-2.jpg';
import img21 from '../assets/albumPics/Migs _ Ressa-21.jpg';
import img30 from '../assets/albumPics/Migs _ Ressa-30.jpg';
import img33 from '../assets/albumPics/Migs _ Ressa-33.jpg';
import img35 from '../assets/albumPics/Migs _ Ressa-35.jpg';
import img36 from '../assets/albumPics/Migs _ Ressa-36.jpg';
import img37 from '../assets/albumPics/Migs _ Ressa-37.jpg';
import img39 from '../assets/albumPics/Migs _ Ressa-39.jpg';
import img40 from '../assets/albumPics/Migs _ Ressa-40.jpg';
import img43 from '../assets/albumPics/Migs _ Ressa-43.jpg';
import img48 from '../assets/albumPics/Migs _ Ressa-48.jpg';
import img50 from '../assets/albumPics/Migs _ Ressa-50.jpg';
import img51 from '../assets/albumPics/Migs _ Ressa-51.jpg';
import img52 from '../assets/albumPics/Migs _ Ressa-52.jpg';

let data = [
  {
    imgSrc: img1
  },
  {
    imgSrc: img39
  },
  {
    imgSrc: img21
  },
  {
    imgSrc: img30
  },
  {
    imgSrc: img33
  },
  {
    imgSrc: img35
  },
  {
    imgSrc: img36
  },
  {
    imgSrc: img37
  },
  {
    imgSrc: img2
  },
  {
    imgSrc: img40
  },
  {
    imgSrc: img43
  },
  {
    imgSrc: img48
  },
  {
    imgSrc: img50
  },
  {
    imgSrc: img51
  },
  {
    imgSrc: img52
  },
  /*{
    imgSrc: img59
  },
  {
    imgSrc: img61
  },
  {
    imgSrc: img62
  },
  {
    imgSrc: img63
  },
  {
    imgSrc: img67
  },
  {
    imgSrc: img70
  },
  {
    imgSrc: img71
  },
  {
    imgSrc: img73
  },
  {
    imgSrc: img75
  },
  {
    imgSrc: img77
  },
  {
    imgSrc: img80
  },
  {
    imgSrc: img81
  },
  {
    imgSrc: img82
  },
  {
    imgSrc: img83
  },
  {
    imgSrc: img84
  },
  {
    imgSrc: img85
  },
  {
    imgSrc: img89
  },
  {
    imgSrc: img93
  },
  {
    imgSrc: img94
  },
  {
    imgSrc: img95
  },
  {
    imgSrc: img99
  },
  {
    imgSrc: img103
  },
  {
    imgSrc: img105
  },
  {
    imgSrc: img107
  },
  {
    imgSrc: img108
  },
  {
    imgSrc: img109
  },
  {
    imgSrc: img111
  },
  {
    imgSrc: img112
  },
  {
    imgSrc: img113
  },
  {
    imgSrc: img114
  },
  {
    imgSrc: img117
  },
  {
    imgSrc: img118
  },
  {
    imgSrc: img120
  },
  {
    imgSrc: img121
  },
  {
    imgSrc: img122
  },
  {
    imgSrc: img131
  },
  {
    imgSrc: img136
  },
  {
    imgSrc: img152
  },
  {
    imgSrc: img158
  },
  {
    imgSrc: img159
  },
  {
    imgSrc: img160
  },
  {
    imgSrc: img162
  },
  {
    imgSrc: img163
  },
  {
    imgSrc: img167
  },
  {
    imgSrc: img172
  },
  {
    imgSrc: img176
  },
  {
    imgSrc: img177
  },
  {
    imgSrc: img180
  },
  {
    imgSrc: img204
  },
  {
    imgSrc: img206
  },
  {
    imgSrc: img244
  },
  {
    imgSrc: img255
  },
  {
    imgSrc: img256
  },
  {
    imgSrc: img257
  },
  {
    imgSrc: img259
  },
  {
    imgSrc: img260
  },
  {
    imgSrc: img262
  },
  {
    imgSrc: img264
  },
  {
    imgSrc: img265
  },
  {
    imgSrc: img266
  },
  {
    imgSrc: img267
  },
  {
    imgSrc: img275
  },
  {
    imgSrc: img280
  }*/
];

export default data;


