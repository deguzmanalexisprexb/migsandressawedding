import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import navsBg from '../assets/background/4.JPG';

const Navs = () => {

    // Check if the user's device is windows or android
    const isAndroid = /Android/i.test(navigator.userAgent);
    const isWindows = /Windows/.test(navigator.userAgent);
    
    return (
        <section
            className="pt-md-5 text-center min-vh-100 align-items-center d-flex"
            style={{
                backgroundImage:
                    `linear-gradient(to top, rgba(600, 600, 600, 0.6), rgba(600, 600, 600, 0.4)), url(${navsBg})`,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundAttachment: isAndroid || isWindows ? 'fixed' : 'scroll',
            }}
        >
        	<Container className='mt-5 dancingScript'>
                <Row className='justify-content-center'>
                    <Col md={3}>
                        <Link to='/' className='nav-link mb-3'><span className="d-inline-block border-bottom border-black border-3 h3">Home</span></Link>
                        <Link to='/venue' className='nav-link mb-3'><span className="d-inline-block border-bottom border-black border-3 h3">Venue</span></Link>
                        <Link to='/party' className='nav-link mb-3'><span className="d-inline-block border-bottom border-black border-3 h3">Dress Code</span></Link>
                        <Link to='/giftGuide' className='nav-link mb-3'><span className="d-inline-block border-bottom border-black border-3 h3">Gift Guide</span></Link>
                        <Link to='/entourage' className='nav-link mb-3'><span className="d-inline-block border-bottom border-black border-3 h3">Entourage</span></Link>
                        <Link to='/gallery' className='nav-link mb-3'><span className="d-inline-block border-bottom border-black border-3 h3">Gallery</span></Link>
                        <Link to='/faqs' className='nav-link mb-5'><span className="d-inline-block border-bottom border-black border-3 h3">FAQs</span></Link>
                    </Col>
                </Row>
        		

                <h2 className='cormorantGaramond fw-bold my-3 text-danger'>THIS IS A PRIVATE INVITATION</h2>
                <p className='cormorantGaramond fw-bold my-3 px-sm-5'>We ask you to keep this invitation private. Our wedding will be a small, intimate ceremony, and only those who are closest to us are expected to be in attendance. Please do not share this to anyone. Thank you</p>
        	</Container>
    	</section>
    );
};

export default Navs;