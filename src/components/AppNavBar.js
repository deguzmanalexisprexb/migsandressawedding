import { Navbar, Container, Nav } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import { useState } from 'react'

const AppNavBar = () => {
	//states
	const [expanded, setExpanded] = useState(false);

	const handleNavClick = () => {
	  setExpanded(false);
	};
    return (
		<Navbar 
			expand="lg" 
			className='shadow'
			style={{
			background: 'linear-gradient(to right, #ACC4DA, #ACC4DA)', zIndex: '123'
			}}
			fixed="top"
		>
		    <Container fluid className='mx-3 text-center'>
    	        <Navbar.Brand 
    	        	as = {Link} 
    	        	to = '/' 
    	        	className='fw-bold cormorantGaramond px-0' 
    	        	style={{color: 'white', fontSize: '2rem', padding: 0}}
    	        >
	        		Migs 
	        		<span className='dancingScript' style={{fontSize: '1.4rem'}} > and </span> 
	        		Ressa
    	        </Navbar.Brand>
    	        <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => setExpanded(!expanded)} style={{color: 'transparent'}}/>
    	        <Navbar.Collapse id="basic-navbar-nav" in={expanded}>
    	          <Nav className="ms-auto cormorantGaramond fw-bold d-inline-block d-lg-flex" onClick={handleNavClick}>
    	               	            
    	            <Nav.Link 
    	              as = {NavLink} 
    	              to = '/'
    	              className="nav-link mt-3 mt-lg-0"
    	              activeclassname="active" 
    	            >
    	              Home
    	            </Nav.Link>
    	            <Nav.Link 
						as = {NavLink} 
						to = '/venue'
						className="nav-link"
						activeclassname="active"
    	            >
						Venue
    	            </Nav.Link>
    	            <Nav.Link 
						as = {NavLink} 
						to = '/party'
						className="nav-link"
						activeclassname="active"
	                >
						Dress Code
	                </Nav.Link>
	                <Nav.Link 
						as = {NavLink} 
						to = '/giftGuide'
						className="nav-link"
						activeclassname="active"
	                >
						Gift Guide
	                </Nav.Link>
	                <Nav.Link 
						as = {NavLink} 
						to = '/entourage'
						className="nav-link"
						activeclassname="active"
	                >
						Entourage
	                </Nav.Link>
	                <Nav.Link 
						as = {NavLink} 
						to = '/gallery'
						className="nav-link"
						activeclassname="active"
	                >
						Gallery
	                </Nav.Link>
	                <Nav.Link 
						as = {NavLink} 
						to = '/faqs'
						className="nav-link"
						activeclassname="active"
	                >
						FAQs
	                </Nav.Link>
    	            
    	          </Nav>
    	        </Navbar.Collapse>
		    </Container>
		</Navbar>
    );
};

export default AppNavBar;