import { useState } from 'react';
import { useSwipeable } from 'react-swipeable';
import '../layouts/album.css';

import { GrClose, GrNext, GrPrevious } from 'react-icons/gr';

import albumData from '../stores/AlbumData';

const Album = () => {

	const data = albumData;

	const [ model, setModel ] = useState(false);
	const [ tempImgSrc, setTempImgSrc ] = useState('');
	const [currentImgIndex, setCurrentImgIndex] = useState(0);

	const nextImg = () => {
	    const nextIndex = (currentImgIndex + 1) % data.length;
	    setTempImgSrc(data[nextIndex].imgSrc);
	    setCurrentImgIndex(nextIndex);
	};

	const prevImg = () => {
	    const prevIndex = (currentImgIndex - 1 + data.length) % data.length;
	    setTempImgSrc(data[prevIndex].imgSrc);
	    setCurrentImgIndex(prevIndex);
	};

	const getImg = ( imgSrc, index ) => {
		setTempImgSrc( imgSrc );
		setCurrentImgIndex(index);
		setModel( true );
	}

	const closeImg = () => {
		setModel(false);
		setTempImgSrc('');
	}

	const handlers = useSwipeable({
		onSwiped: (eventData) => {
		    if (eventData.dir === 'Left') {
				nextImg(); // Swipe left to show the next image
		    } else if (eventData.dir === 'Right') {
				prevImg(); // Swipe right to show the previous image
		    }
		},
	});

    return (
    	<>
	    	<div className={model? 'model open' : 'model'}  onClick={() => closeImg()} onTouchMove={() => closeImg()}>
	    		<div {...handlers}>
	    			<img src={tempImgSrc} alt='largeImage' onClick={(e) => e.stopPropagation()} onTouchMove={(e) => e.stopPropagation()}/>
	    		</div>
				<GrClose className='closeButton'/>
				<GrPrevious className='prevButton btn' onClick={(e) => { e.stopPropagation(); prevImg(); }} />
				<GrNext className='nextButton btn' onClick={(e) => { e.stopPropagation(); nextImg(); }}/>
	    	</div>
	    	
			<div className='gallery'>
				{ data.map((item, index) => {
					return( <div className='pics' key={index} onClick={()=> getImg(item.imgSrc, index)} >
						<img src={item.imgSrc} style={{width: '100%'}} alt={`${index + 1}`} />
					</div>)
				})}
			</div>
		{/*hello world*/}
    	</>
    );
};

export default Album;