import { Container, Row, Col } from 'react-bootstrap';
import prenup from '../assets/prenup2.mp4';
import Timer from './Timer';

import React from 'react';

const Video = () => {
	// Calculate the target date in UTC and adjust for UTC+8 (Philippine Time)
	const targetDateUTC = new Date(Date.UTC(2023, 10, 14, 14, 0, 0)); // Months are zero-based, so 7 represents August
	targetDateUTC.setHours(targetDateUTC.getHours() - 8); // Subtract 8 hours to adjust for UTC+8

	// Format the targetDateUTC for display
	const targetDate = targetDateUTC.toISOString(); // This is the adjusted target date in ISO format

    return (
    	<div 
		className="text-center justify-content-center" 
		style={{ 
			width: '100%', 
			height: '80vh', 
			position: 'relative' 
		}}>
    		<div 
			style={{
				position: 'absolute', 
				top: '0', 
				left: '0', 
				width: '100%', 
				height: '100%', 
				backgroundColor: 'rgba(100,100,100,0.5)'
			}}/>
			<video autoPlay loop muted playsInline style={{ width: '100%', height: '100%', objectFit: 'cover' }}>
	    	    <source src={prenup} type="video/mp4"/>
			</video>
			<Container 
			className="d-flex flex-column justify-content-center" 
			style={{ 
				width: '100%', 
				height: '100%', 
				maxWidth: '100%', 
				position: 'absolute', 
				top: '0' 
			}}>
	    	    <Row>
					<Col md={10} className="mx-auto">
		    	    	<Timer targetDate={targetDate}/>
		    	    </Col>
	    	    </Row>
			</Container>
    	</div>
    );
};

export default Video;